package xyz.sebastienm4j.moorgate.app

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.builder.SpringApplicationBuilder
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer

private fun defaultProperties() : Map<String, Any> {
    val defaultProperties = HashMap<String, Any>()
    defaultProperties["spring.profiles.active"] = "dev"
    defaultProperties["server.port"] = 12280
    return defaultProperties
}

@SpringBootApplication
class MoorgateApp : SpringBootServletInitializer() {

    override fun configure(builder: SpringApplicationBuilder) : SpringApplicationBuilder {
        builder.sources(MoorgateApp::class.java)
        builder.application().setDefaultProperties(defaultProperties())
        return builder
    }

}

fun main(args: Array<String>) {
    val application = SpringApplication(MoorgateApp::class.java)
    application.setDefaultProperties(defaultProperties())
    application.run(*args)
}
